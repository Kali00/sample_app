class SessionsController < ApplicationController
  def new
  end

  def create
    #look if user exist by checking his email address
    @user = User.find_by(email: params[:session][:email].downcase)
    if @user && @user.authenticate(params[:session][:password])
      if @user.activated?
        log_in @user 
        params[:session][:remember_me] == '1' ? remember(@user) : forget(@user)
        redirect_back_or @user
      else
        message = "Account not activ"
        message += "Please check your email for activation link"
        flash[:warning] = message
        redirect_to root_url
      end
      #log the user in
      # log_in @user #if email is okay log in
      # params[:session][:remember_me] == '1' ? remember(@user) : forget(@user)
      # redirect_back_or @user #user page url
    else
      #create error message
      flash.now[:danger] = "Invalid email or password"
      render 'new'
    end
  end #so many problems because of missing "end" :D
  # dont forget that also if need to "end"!!

  def destroy
    log_out if logged_in?#from session helper|check if user is logged in in another window
    redirect_to root_url
  end

end