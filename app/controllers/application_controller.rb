class ApplicationController < ActionController::Base
  protect_from_forgery  with: :exception
  include SessionsHelper
  
# def text
# 	render html: "it's some text I put here just to follow the book instructions"
# end

private
#confirms logged-in user


  def logged_in_user
    unless logged_in?
      store_location
      flash[:danger] = "please log in to post"
      redirect_to login_url
    end
  end

end
