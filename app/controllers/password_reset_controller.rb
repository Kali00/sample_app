class PasswordResetsController < ApplicationController

  before_action :get_user, only: [:edit, :update]
  before_action :valid_user, only: [:edit, :update]
  #below: an expired password reset
  before_action :check_expiration, only: [:edit, :update]

  def new
  end

  def create
    @user = User.find_by(email: params[:password_reset][:email].downcase)
    if @user
      @user.create_reset_digest
      @user.send_password_reset_email
      flash[:info] = "Email sent with password reset instructions"
      redirect_to root_url
    else
      flash.now[:danger] = "Email address not found"
      render 'new'
    end
  end

  def edit
  end

  def update
    #one line below: failed update due to an empty pass and cofirmation
    if params[:user][:password].empty?
      @user.errors.add(:password, "cant be empty")
      render 'edit'
      #one line below: successful update 
    elsif @user.update_attributes(user_params)
      log_in @user
      @user.update_attribute(:reset_digest, nil)
      flash[:success] = "password has been reset"
      redirect_to @user
    else
      render 'edit' #failed update due to invalid pass
    end
  end

  private 

  def user_params
    params.reqire(:user).permit(:password, :password_confirmation)
  end

  def get_user
    @user = User.find_by(email: params[:email])
  end

  #confirm valid user

  def valid_user 
    unless (@user && @user.activated? &&
            @user.authenticated?(:reset, params[:id]))
    redirect_to root_url
    end
  end

  #check expiration of reset token
  def check_expiration 
    if @user.password_reset_expired?
      flash[:danger] = "password reset has been expired"
      redirect_to new_password_reset_url
    end
  end
  
end