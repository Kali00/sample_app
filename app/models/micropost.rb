class Micropost < ApplicationRecord
  belongs_to :user
  default_scope -> { order(created_at: :desc)} #from newest to oldest posts
  # mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :cotent, presence: true, length: { maximum: 140 }

end
